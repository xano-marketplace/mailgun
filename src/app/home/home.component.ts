import {Component, OnInit} from '@angular/core';
import {ConfigService, XanoConfig} from "../_demo-core/config.service";
import {MailgunService} from "../api-services/mailgun.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {BehaviorSubject} from "rxjs";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
	public config: XanoConfig;
	public configured: boolean = false;
	public templates;
	public data = new BehaviorSubject([{key: '', value: ''}]);
	public form: FormGroup = new FormGroup({
		to_email: new FormControl('', Validators.required),
		subject: new FormControl(),
		body: new FormControl(),
		template: new FormControl('none'),
		data: new FormControl()
	});

	constructor(
		private configService: ConfigService,
		private mailgunService: MailgunService,
		private snackBar: MatSnackBar
	) {
	}

	ngOnInit(): void {
		this.config = this.configService.config;
		this.configService.xanoApiUrl.subscribe(apiUrl => this.configured = !!apiUrl);

		this.mailgunService.templates(null).subscribe(res => {
			this.templates = res;
		}, error => this.configService.showErrorSnack(error))
	}

	resetForm(): void {
		this.form.reset();
		this.form.controls.to_email.setErrors(null);
		this.form.controls.template.patchValue('none');
	}

	addData() {
		this.data.next([...this.data.value, {key: '', value: ''}]);
	}

	removeData(rowID) {
		let data = [...this.data.value];
		data.splice(rowID, 1);
		this.data.next(data);
	}

	public send() {
		this.form.markAllAsTouched()
		if (this.form.valid) {
			let payload = this.form.getRawValue();
			if (this.form.controls.template.value === 'none') {
				payload.template = null;
			} else {
				let data = {};
				for (let item of this.data.value) {
					if (item.key !== '' && item.value !== '') {
						data[item.key] = item.value;
					}
				}
				payload["data"] = data;
			}

			this.mailgunService.send(payload).subscribe(res => {
				if (res) {
					this.snackBar.open('Message Queued', 'Success');
					this.resetForm();
					this.form.controls.to_email.markAsUntouched();
				}
			}, error => this.configService.showErrorSnack(error))
		}
	}
}
