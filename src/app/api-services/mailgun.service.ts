import {Injectable} from '@angular/core';
import {ConfigService} from "../_demo-core/config.service";
import {ApiService} from "../_demo-core/api.service";
import {Observable} from "rxjs";

@Injectable({
	providedIn: 'root'
})
export class MailgunService {
	constructor(
		private configService: ConfigService,
		private apiService: ApiService
	) {
	}

	public send(payload): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/mailgun/send`,
			params: payload
		});
	}


	public templates(paging: { limit: number, page: string, pivot: string }): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/mailgun/templates`,
			params: paging
		});
	}
}
