import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {DemoLandingComponent} from "./_demo-core/demo-landing/demo-landing.component";
import {ConfigGuard} from "./_demo-core/config.guard";

const routes: Routes = [
	{path: '', component: DemoLandingComponent, data: {animation: 'demoLandingPage'}},
	{path: 'app', component: HomeComponent, canActivate: [ConfigGuard]}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
