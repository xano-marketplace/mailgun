import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {XanoService} from "./xano.service";
import {ApiService} from "./api.service";
import {get} from 'lodash-es';
import {MatSnackBar} from "@angular/material/snack-bar";

export interface XanoConfig {
	title: string,
	summary: string,
	editText: string,
	editLink: string,
	descriptionHtml: string,
	components: any,
	instructions: any;
	logoHtml: string,
	requiredApiPaths: string[]
}

@Injectable({
	providedIn: 'root'
})


export class ConfigService {
	public xanoApiUrl: BehaviorSubject<any> = new BehaviorSubject<any>(null);
	public config: XanoConfig = {
		title: 'Mailgun',
		summary: 'This demo demonstrates the usage of the Mailgun Extension.',
		editText: 'Get source code',
		editLink: 'https://gitlab.com/mailgun',
		components: [
			{
				name: 'Home / Send',
				description: 'This component allows you to send either a text based or template based email using Mailgun. '
			},
		],
		instructions: [
			`Install the template in your Xano Workspace`,
			`Create a <a href="https://www.mailgun.com/" target="_blank">Mailgun</a> account or use an existing one (be sure to activate your account)`,
			`Go to <a href="https://app.mailgun.com/app/sending/domains" target="_blank">https://app.mailgun.com/app/sending/domains</a> 
				and select the domain you want to use (for this demo your sandbox one is fine, otherwise follow the steps for
				 <a href="https://help.mailgun.com/hc/en-us/articles/203637190-How-Do-I-Add-or-Delete-a-Domain-" target="_blank">setting up your personal domain)</a>`,
			`Next, in your <a href="https://app.mailgun.com/app/sending/domains" target="_blank">domains page</a> on the right hand side add your email you want to send to in 
			your authorized recipient`,
			`Mailgun requires that you confirm your email that you are setting as an authorized recipient, so check the email you provided and verify`,
			`Next, you\'ll need to set some environment variables. In your <a href="https://app.mailgun.com/app/sending/domains" target="_blank">domains page</a> click <b>Select</b>
			 copy the API key and set it as <b>mailgun_private_key</b> in your Xano workspace settings. Next set <b>mailgun_domain</b> to the value of your mailgun domain (if you're
			 using the sandbox it will look something like <b>sandboxRANDOMSTRING.mailgun.org</b>, finally set <b>mailgun_from_email</b> which can be any email of your choosing.
			`,
			`Finally go to your Xano workspace that you installed this extension and under the API group Mailgun copy the "Base Request URL" and
 			paste this in as <b>"Your Xano API URL"</b> on this page.`,
			`(Optional) if you'd like to use a dynamic email, create a new template in your Mailgun dashboard, then, in this demo,
			 select the template from the drop down and set your variables in the table.`

		],
		descriptionHtml: ``,
		logoHtml: '',
		requiredApiPaths: [
			'mailgun/templates',
			'/mailgun'
		]
	};

	constructor(
		private apiService: ApiService,
		private xanoService: XanoService,
		private snackBar: MatSnackBar) {
	}

	public isConfigured(): Observable<any> {
		return this.xanoApiUrl.asObservable();
	}

	public configGet(apiUrl): Observable<any> {
		return this.apiService.get({
			endpoint: this.xanoService.getApiSpecUrl(apiUrl),
			headers: {
				Accept: 'text/yaml'
			},
			responseType: 'text',
		});
	}

	public showErrorSnack(error) {
		this.snackBar.open(get(error, 'error.message', 'An error has occurred'), 'Error', {panelClass: 'error-snack'});
	}

}
