import {Component, OnInit} from '@angular/core';
import {ConfigService, XanoConfig} from '../_demo-core/config.service';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {
	public apiConfigured: boolean = false;
	public config: XanoConfig;

	constructor(
		private configService: ConfigService,
	) {
	}

	ngOnInit(): void {
		this.config = this.configService.config;
		this.configService.isConfigured().subscribe(apiUrl => {
			this.apiConfigured = !!apiUrl;
		});
	}

}
